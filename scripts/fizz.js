/* File: fizz.js
 * Author:Duy Trinh
 */

/* Description: Write a program that uses document.write to print all the numbers fro 1 too 100.  For numbers divisible by 3, print "Fizz" instead fo the number. Keep track of how many times "Fizz" is printed and write that out to the document. (5 marks)*/
var track3 = 0;
for (var count = 1; count <= 100; count++){
	if ((count % 3) == 0){
		document.write("Fizz");
		track3++;
	}else{
		document.write(count);
	}

	if ((count % 20) == 0){
		document.write(", <br/>");
	}else{
		document.write(", ");
	}
}
document.write("<br/> There was " + track3 + " numbers that are divisible by 3.");