/* File: fizzbuzz.js
 * Author:Duy Trinh
 */

/*
Description: Now modify the program to print "FizzBuzz" for numbers that are divisible by both 3 and 5 (and still print "Fizz" or "Buzz" for numbers divisible by only one of those).  Keep count of how many times "Fizz", "Buzz" and "FizzBuzz" are written, and write those counts out to the document. (5 marks)
*/
var track3 = 0;
var track5 = 0;
var track35 = 0;

for (var count = 1; count <= 100; count++){
	
	if ((count % 3) == 0 && (count % 5) == 0){
		document.write("FizzBuzz");
		track35++;
	}else if ((count % 3) == 0){
		document.write("Fizz");
		track3++;
	}else if ((count % 5) == 0){
		document.write("buzz");
		track5++;
	}else{
		document.write(count);
	}

	if ((count % 20) == 0){
		document.write(", <br/>");
	}else{
		document.write(", ");
	}
}
document.write("<br/> There was " + track3 + " numbers that are divisible by 3 but not by 5, " + track5 + " numbers that are divisible by 5 but not by 3. and " + track35 + " numbers that are divisible by both 3 and 5.<br/>");
