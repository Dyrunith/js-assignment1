/* File: buzz.js
 * Author:Duy Trinh
 */

/* Description: Write a program that uses document.write to print all the numbers fro 1 too 100.  For numbers divisible by 3, print "Fizz" instead fo the number, and for numbers divisible by 5 (and not 3), print "Buzz" instead. Keep track of how many time Fizz is printed, and how many times Buzz is printed.  Print the counts for both of these. (5 marks)*/

var track3 = 0;
var track5 = 0;

for (var count = 1; count <= 100; count++){
	
	if ((count % 3) == 0){
		document.write("Fizz");
		track3++;
	}else if ((count % 5) == 0){
		document.write("buzz");
		track5++;
	}else{
		document.write(count);
	}

	if ((count % 20) == 0){
		document.write(", <br/>");
	}else{
		document.write(", ");
	}
}
document.write("<br/> There was " + track3 + " numbers that are divisible by 3, and " + track5 + " numbers that are divisible by 5 but not by 3.<br/>");

