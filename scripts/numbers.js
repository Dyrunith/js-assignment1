/* File: numbers.js
 * Author: Duy Trinh
 */

/* Description: Write a program that uses document.write to print all the numbers from 1 too 100 (5 marks) */

// This is a for loop that will print 1 to 100 in the broswer
for (var count = 1; count <= 100; count++){
	if ((count % 20) == 0){
		document.write(count + ", <br/>");
	}else{
		document.write(count + ", ");
	}
}